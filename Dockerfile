FROM alpine
ADD . /app
RUN apk add python3 sqlite py3-setuptools --no-cache
RUN mkdir /data
RUN cd /app ; python3 setup.py install
RUN rm -rf /app
CMD cd /data ; python3 -m pypastebin 8000
